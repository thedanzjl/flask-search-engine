from urllib.parse import quote
from collections import Counter
from queue import Queue
from bs4 import BeautifulSoup
from bs4.element import Comment
import urllib.parse
import requests
import nltk
import re


class Document:

    def __init__(self, url):
        self.url = url

    def get(self):
        if not self.load():
            if not self.download():
                raise FileNotFoundError(self.url)
            else:
                self.persist()

    def download(self):
        # print("Calling download")
        try:
            self.content = requests.get(self.url, verify=False).content
            # print("Downloaded")
            return True
        except Exception as e:
            return False

    def persist(self):
        with open('docs/' + quote(self.url).replace('/', '_'), 'wb') as f:
            f.write(self.content)

    def load(self):
        # print("Loading from drive")
        try:
            with open('docs/' + quote(self.url).replace('/', '_'), 'rb') as f:
                self.content = f.read()
            # print("Loaded from drive")
            return True
        except:
            return False


class HtmlDocument(Document):

    def normalize(self, href):
        if href is not None and href[:4] != 'http':
            href = urllib.parse.urljoin(self.url, href)
        return href

    def parse(self):

        def tag_visible(element):
            if element.parent.name in ['style', 'script', 'head', 'title', 'meta', '[document]']:
                return False
            if isinstance(element, Comment):
                return False
            return True

        model = BeautifulSoup(self.content, "html.parser")

        self.anchors = []
        a = model.find_all('a')
        for anchor in a:
            href = self.normalize(anchor.get('href'))
            text = anchor.text
            self.anchors.append((text, href))

        self.images = []
        i = model.find_all('img')
        for img in i:
            href = self.normalize(img.get('src'))
            self.images.append(href)

        texts = model.findAll(text=True)
        visible_texts = filter(tag_visible, texts)
        self.text = u" ".join(t.strip() for t in visible_texts)


class HtmlDocumentTextData:

    def __init__(self, url):
        self.doc = HtmlDocument(url)
        self.doc.get()
        self.doc.parse()

    def get_sentences(self):
        nltk.download('punkt')
        sentences = nltk.tokenize.sent_tokenize(self.doc.text)
        result = []
        for sent in sentences:
            for line in sent.split('\n'):
                if line.strip():
                    result.append(line.strip())
        return result

    def get_word_stats(self):
        return Counter([word.lower() for word in re.split('\W', self.doc.text) if word and not word[0].isdigit()])


class Crawler:

    def crawl_generator(self, source, depth=1):
        q = Queue()
        q.put((source, 0))
        visited = set()
        while not q.empty():
            url, url_depth = q.get()
            if url not in visited:
                visited.add(url)
                try:
                    doc = HtmlDocumentTextData(url)
                    for a in doc.doc.anchors:
                        if url_depth + 1 < depth:
                            q.put((a[1], url_depth + 1))
                    yield doc,
                except FileNotFoundError as e:
                    print("Analyzing", url, "led to FileNotFoundError")
                    yield 'FileNotFoundError', url  # the doc was removed


def main():
    crawler = Crawler()
    counter = Counter()

    for c in crawler.crawl_generator("https://university.innopolis.ru/en/", 2):
        c = c[0]
        print(c.doc.url)
        if c.doc.url[-4:] in ('.pdf', '.mp3', '.avi', '.mp4', '.txt'):
            print("Skipping", c.doc.url)
            continue
        counter.update(c.get_word_stats())
        print(len(counter), "distinct word(s) so far")

    print("Done")

    print(counter.most_common(20))
    assert [x for x in counter.most_common(20) if x[0] == 'innopolis'], 'innopolis sould be among most common'


if __name__ == '__main__':
    main()