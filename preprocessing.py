from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
from bs4 import BeautifulSoup
from pytrie import StringTrie as Trie
import Levenshtein as lev
import re
import string
import nltk
import requests
import tarfile
import os
import ssl

try:
    _create_unverified_https_context = ssl._create_unverified_context
except AttributeError:
    pass
else:
    ssl._create_default_https_context = _create_unverified_https_context

nltk.download('stopwords')
nltk.download('wordnet')
nltk.download('punkt')


def normalize(text: str):
    text = text.lower()
    result = re.sub(r'\d+', '', text)
    punctuation = ''.join([p for p in string.punctuation if p != '*'])  # allow '*' sign
    table = str.maketrans(punctuation, ' ' * len(punctuation))
    result = result.translate(table)
    result = result.strip()
    return result


def tokenize(text):
    tokens = nltk.word_tokenize(text)
    return tokens


def lemmatization(tokens):
    lemmatizer = WordNetLemmatizer()
    return list(map(lambda token: lemmatizer.lemmatize(token, pos='v'), tokens))


def remove_stop_word(tokens):
    stop_words = set(stopwords.words('english'))
    return [token for token in tokens if token not in stop_words]


def preprocess(text):
    text = normalize(text)
    tokens = tokenize(text)
    lemmed = lemmatization(tokens)
    clean = remove_stop_word(lemmed)
    return clean


def download_reut():
    target_path = 'docs/reuters21578.tar.gz'

    response = requests.get(
        'https://archive.ics.uci.edu/ml/machine-learning-databases/reuters21578-mld/reuters21578.tar.gz'
        , stream=True)
    if response.status_code == 200:
        with open(target_path, 'wb') as f:
            f.write(response.raw.read())
    else:
        print(f'status code {response.status_code}')
        return

    tar = tarfile.open(target_path, "r:gz")
    tar.extractall()
    tar.close()


def get_collection():
    collection = []
    for f in os.listdir(os.getcwd()):
        if f.startswith('reut2'):
            try:
                with open(os.path.join(os.getcwd(), f), 'rb') as reut:
                    soup = BeautifulSoup(reut, 'html.parser')
                    bodies = soup.find_all('body')
                    bodies = [body.string for body in bodies]
                    collection.extend(bodies)
            except UnicodeDecodeError:
                print(f'Error parsing {f}')
    return collection


def make_index(collection):
    inverted_index = {}
    for doc_i, text in enumerate(collection):
        terms = preprocess(text)
        for term in terms:
            if term in inverted_index:
                inverted_index[term].add(doc_i)
            else:
                inverted_index[term] = {doc_i}
    return inverted_index


def soundex(query: str):
    """
    Taken from https://medium.com/@yash_agarwal2/soundex-and-levenshtein-distance-in-python-8b4b56542e9e
    """

    # Step 0: Clean up the query string
    query = query.lower()
    letters = [char for char in query if char.isalpha()]
    assert len(letters) > 0, f'not alpha characters in query {query}'

    # Step 1: Save the first letter. Remove all occurrences of a, e, i, o, u, y, h, w.

    # If query contains only 1 letter, return query+"000" (Refer step 5)
    if len(query) == 1:
        return query + "000"

    to_remove = ('a', 'e', 'i', 'o', 'u', 'y', 'h', 'w')

    first_letter = letters[0]
    letters = letters[1:]
    letters = [char for char in letters if char not in to_remove]

    if len(letters) == 0:
        return first_letter + "000"

    # Step 2: Replace all consonants (include the first letter) with digits according to rules

    to_replace = {('b', 'f', 'p', 'v'): 1, ('c', 'g', 'j', 'k', 'q', 's', 'x', 'z'): 2,
                  ('d', 't'): 3, ('l',): 4, ('m', 'n'): 5, ('r',): 6}

    first_letter = [value if first_letter else first_letter for group, value in to_replace.items()
                    if first_letter in group]
    letters = [value if char else char
               for char in letters
               for group, value in to_replace.items()
               if char in group]

    # Step 3: Replace all adjacent same digits with one digit.
    letters = [char for ind, char in enumerate(letters)
               if (ind == len(letters) - 1 or (ind + 1 < len(letters) and char != letters[ind + 1]))]

    # Step 4: If the saved letter’s digit is the same the resulting first digit, remove the digit (keep the letter)
    if first_letter == letters[0]:
        letters[0] = query[0]
    else:
        letters.insert(0, query[0])

    # Step 5: Append 3 zeros if result contains less than 3 digits.
    # Remove all except first letter and 3 digits after it.

    first_letter = letters[0]
    letters = letters[1:]

    letters = [char for char in letters if isinstance(char, int)][0:3]

    while len(letters) < 3:
        letters.append(0)

    letters.insert(0, first_letter)

    string = "".join([str(l) for l in letters])

    return string


def make_soundex_index(inverted_index):
    """
    Format of soundex_index: {'r163': ['rupert', 'robert', ...], ...}
    """
    soundex_index = {}
    for term in inverted_index:
        try:
            code = soundex(term)
        except Exception as e:
            print(term, str(e))
            continue
        if code in soundex_index:
            soundex_index[code].add(term)
        else:
            soundex_index[code] = {term}
    return soundex_index


def levenshtein_distance(string1: str, string2: str):
    return lev.distance(string1, string2)
