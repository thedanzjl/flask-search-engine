# flask search engine

IR lab 4 - basic search engine on flask

## running locally

1.  make sure you have postings/ and docs/ directories inside the project directory `mkdir postings & mkdir docs`
2.  install dependecies via `pip install -r requirements.txt`
3.  run code `python app.py`