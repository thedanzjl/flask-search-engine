from flask import Flask, request, render_template, redirect
from threading import Thread
from crawler import Crawler
from collections import Counter
from flask_paginate import Pagination, get_page_parameter
import preprocessing
import shutil, os

app = Flask(__name__)

inverted_index = Counter()
aux_index = Counter()
removed = set()
prefix_tree = None
soundex_index = None


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/search')
def search_results():
    query = request.args.get('q', '')
    if query:
        results = search(query)
    else:
        return redirect('/')
    page = request.args.get(get_page_parameter(), type=int, default=1)

    pagination = Pagination(page=page, total=len(results), search=True, record_name='results')
    pagination.per_page = 20
    return render_template('search_results.html', q=query, results=results, pagination=pagination)


def search(query):
    terms = preprocessing.preprocess(query)
    # 1. Make resultant_query out of initial query

    resultant_query = list()
    for term in terms:
        if term in prefix_tree:
            print('in prefix tree: ', term)
            resultant_query.append(term)
        else:  # that's where the fun part begins... :)
            if '*' in term:
                print('* in: ', term)
                term = term.replace('*', '')
                replacementes = prefix_tree.keys(prefix=term)
                resultant_query.append(replacementes)
            else:
                print('processing term: ', term)
                soundex_ = preprocessing.soundex(term)
                similars = soundex_index.get(soundex_)
                print('similars: ', similars)
                if similars is None:
                    continue
                lev_similars = {s: preprocessing.levenshtein_distance(term, s) for s in
                                similars}  # similar terms as keys with Levenshtein dist as values
                print('levenshtein similars: ', lev_similars)
                min_distance = min(lev_similars.values())
                min_lev_similars = [similar for similar, dist in lev_similars.items() if dist == min_distance]
                min_lev_similars = preprocessing.remove_stop_word(min_lev_similars)
                print('similars with min levenshtein distance (and not in stop words): ', min_lev_similars)
                if len(min_lev_similars) > 0:
                    resultant_query.append(min_lev_similars)

    # 2. Find relevants documents

    # [term1, [term2.0, term2.1, term2.2], term3] =>
    # index[term1] INTERS (index[term2.0] UNION index[term2.1] UNION index[term2.2]) INTERS index[term3]

    relevants = list()
    for term in resultant_query:
        if isinstance(term, list):
            result_docs = get_docs_by_term(term[0])
            if len(term) > 1:
                for subterm in term[1:]:
                    result_docs = result_docs.union(get_docs_by_term(subterm))
            relevants.append(result_docs)
        else:
            relevants.append(get_docs_by_term(term))

    docs = relevants[0].intersection(*relevants[1:])
    return list(docs)


def crawling(clear_posting=False):
    global inverted_index, aux_index
    if clear_posting:
        shutil.rmtree('postings/')
        os.mkdir('postings/')

    crawler = Crawler()

    for c in crawler.crawl_generator("https://www.geeksforgeeks.org/python-string-isalpha-application/", 3):
        if len(c) == 2 and c[0] == 'FileNotFoundError':  # the doc was removed
            removed.add(c[1])  # c[1] is url of removed doc
            continue
        c = c[0]
        if c.doc.url[-4:] in ('.pdf', '.mp3', '.avi', '.mp4', '.txt'):
            continue
        url = c.doc.url
        words = c.get_word_stats()
        # add new words to aux_index
        aux_index.update(words)
        # create separate file for every word (posting)
        for word in words:
            with open('postings/' + word + '.txt', 'a+') as posting:
                posting.write(url + ';')
        if len(aux_index) > 1000 or len(inverted_index) == 0:
            manage_indexes()


def manage_indexes():
    """
    Merges periodically index and auxiliary index
    """
    global inverted_index, aux_index, prefix_tree, soundex_index
    inverted_index.update(aux_index)
    aux_index = Counter()
    prefix_tree = preprocessing.Trie.fromkeys(inverted_index)
    soundex_index = preprocessing.make_soundex_index(inverted_index)


def get_docs_by_term(term):
    """
    :returns: postings list of term
    """
    assert isinstance(term, str), f'term {term} is not string'
    with open('postings/' + term + '.txt', 'r') as posting:
        result = set(posting.read().split(';'))
        return {doc for doc in result if doc not in removed}  # exclude docs that are removed


def main():
    Thread(target=crawling, kwargs={'clear_posting': False}).start()
    app.run()


if __name__ == '__main__':
    main()
